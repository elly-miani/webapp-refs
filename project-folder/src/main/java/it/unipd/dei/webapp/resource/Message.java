package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import java.io.*



public class Message extends Resource 
{
	// The message
	private final String message;

	// The code of the error, if any
	private final String errorCode;

	// Additional details about the error, if any
	private final String errorDetails;
	
	// Indicates whether the message is about an error or not.
	private final boolean isError;


	// Creates an error message
	public Message(final String message, final String errorCode, final String errorDetails) 
	{
		this.message = message;
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
		this.isError = true;
	}


	// Creates a generic message
	public Message(final String message) 
	{
		this.message = message;
		this.errorCode = null;
		this.errorDetails = null;
		this.isError = false;
	}


	// Returns the message
	public final String getMessage() 
	{
		return message;
	}


	// Returns the code of the error, if any
	public final String getErrorCode() 
	{
		return errorCode;
	}


	// Returns additional details about the error, if any
	public final String getErrorDetails() 
	{
		return errorDetails;
	}


	// Indicates whether the message is about an error or not
	public final boolean isError() 
	{
		return isError;
	}


	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("message");

		jg.writeStartObject();

		jg.writeStringField("message", message);

		if(errorCode != null) {
			jg.writeStringField("error-code", errorCode);
		}

		if(errorDetails != null) {
			jg.writeStringField("error-details", errorDetails);
		}

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}
}
