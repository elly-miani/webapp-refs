<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Head tags and Bootstrap and Fontawesome css imports -->
  <c:import url="/jsp/include/head.jsp" />
  <!-- TODO: Remember to use "<c:url value="/folder/path"/>" for urls-->

  <!-- Custom Stylesheet -->
  <link rel="stylesheet" type="text/css" href="../css/styles.css">

  <title> PAGE TITLE </title>

</head>

<body>
  <p> Main body </p>





  

  <!-- Javascript imports -->
  <c:import url="/jsp/include/foot.jsp" />

  <!-- Custom Javascript -->
  <script type="text/javascript" src="../js/script.js"></script>

</body>

</html>
