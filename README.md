# Cheatsheet and Quick Links
---

[Webapp Notes](https://www.notion.so/Web-App-Content-48d5d94c5c814a0b9133d84121ab5a3f) | [Misc Cheatsheets](https://www.notion.so/Cheatsheets-12bd60c6bc8c4ab4b98cd6917c3dd5c3) | [Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/) | [Fontawesome](https://fontawesome.com/)

[Unipd Repo](https://bitbucket.org/frrncl/webapp-unipd) | [Exercises Repo](https://bitbucket.org/elly-miani/webapp-exercises)

### VIM configuration
---
[Cheatsheet 1](https://devhints.io/vim) | [Cheatsheet 2](https://vim.rtorr.com/)

`:set tabstop=4`
`:syntax on`
`:set background=dark`

Add to `~/.vimrc` file to make default.


### MAVEN
---
[Central Repository Search Engine](https://search.maven.org/)

`mvn [options] [<goal(s)>] [<phase(s)>]`
goals format: `<plugin-name>:<goal-name>`

##### Basic command:
`mvn clean package javadoc:javadoc`



#APP DEVELOPMENT
---
1. Clone remote repo or create new one
2. Create folder structure with `./create-proj`
3. Move folders (eventually change its name to the project name)
4. Update `pom.xml` file as necessary (REMEMBER TO USE UNIQUE NAME FOR LATER DEPLOYMENT)
5. Update `src/main/webapp/WEB-INF/web.xml` to match `<welcome-file>`
6. Write code for project
7. Run `mvn clean package javadoc:javadoc` to compile
8. Connect to `http://dbstud.dei.unipd.it:8080/manager/html/` to deploy


## For server side:
1. Identify which resources are needed and update the `GENERICRESOURCE.java` file as needed
	- Make sure all fields and their corresponding datatypes are filled in consistently
	- Make sure everything is written in the same way as the json file you need to produce
2. Update the files `SearchResourceByParameterDatabase.java` or `CreateResourceDatabase.java` as needed
	- Make sure all fields and their corresponding datatypes are filled in consistently
	- Make sure the SQL statement is correct and has the necessary spacing
	- Make sure to use the correct database schema name
3. Update the files `SearchResourceByParamaterServlet.java` or `CreateResourceServlet.java` as needed
	- Make sure the `content-type` is correct
	- Make sure the error messages and error codes are correct
	- Make sure you choose correctly between `GET` and `POST` methods
	- Make sure all servlets are correctly defined in `web.xml`




### DEPLOYING ON TOMCAT
To connect to DEI and dbstud from outside:
`ssh -L 8081:dbstud.dei.unipd.it:8080 accountname@login.dei.unipd.it`

From browser: `http://localhost:8081/manager/html`

1. Upload `.war` file
2. Click on deploy
3. App will be available at `/project-name-1.00/`


## FINAL CHECKS

- Check project name and ids in:
	- `pom.xml`
	- `web.xml`
	- all `html` and `jsp` pages
- Check dependencies in `pom.xml` file
- Delete server side or client side directories when not needed
- Delete `jsp` pages or `html` template pages based on which one you used

- Make sure all files (css-js-images) are loaded correctly based on the `<welcome-file>` tag in `web.xml`

