package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.*;

import java.io.*;

// Represents the data about a RESOURCE
public class GENERICRESOURCE extends Resource 
{

	// The unique identifier of the RESOURCE
	private final int id;

	// The ATTRIBUTE1 of the RESOURCE
	private final String ATTRIBUTE1;

	// The ATTRIBUTE2 of the RESOURCE
	private final double ATTRIBUTE2;



	// Creates a new RESOURCE
	public RESOURCE(final int id, final String ATTRIBUTE1, final double ATTRIBUTE2) 
	{
		this.id = id;
		this.ATTRIBUTE1 = ATTRIBUTE1;
		this.ATTRIBUTE2 = ATTRIBUTE2;
	}


	// Returns the identifier of the RESOURCE
	public final int getIdentifier() 
	{
		return id;
	}

	// Returns the ATTRIBUTE1 of the RESOURCE
	public final String getATTRIBUTE1() 
	{
		return ATTRIBUTE1;
	}

	// Returns the ATTRIBUTE2 of the RESOURCE
	public final double getATTRIBUTE2() 
	{
		return ATTRIBUTE2;
	}



	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("RESOURCE");

		jg.writeStartObject();

		jg.writeNumberField("id", id);

		jg.writeStringField("ATTRIBUTE1", ATTRIBUTE1);

		jg.writeNumberField("ATTRIBUTE2", ATTRIBUTE2);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}



	// Creates a RESOURCE from its JSON representation.
	public static RESOURCE fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int id = -1;
		String ATTRIBUTE1 = null;
		double ATTRIBUTE2 = -1;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "RESOURCE".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no RESOURCE object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "id":
						jp.nextToken();
						id = jp.getIntValue();
						break;
					case "ATTRIBUTE1":
						jp.nextToken();
						ATTRIBUTE1 = jp.getText();
						break;
					case "ATTRIBUTE2":
						jp.nextToken();
						ATTRIBUTE2 = jp.getDoubleValue();
						break;
				}
			}
		}

		return new RESOURCE(id, ATTRIBUTE1, ATTRIBUTE2);
	}
}

