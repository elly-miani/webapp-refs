package it.unipd.dei.webapp.servlet;

// import databases and resources
import it.unipd.dei.webapp.database.SearchRESOURCEByPARAMETERDatabase;
import it.unipd.dei.webapp.resource.RESOURCE;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


public final class SearchRESOURCEByPARAMETERServlet extends AbstractDatabaseServlet 
{

	// EXAMPLE WITH STRING PARAMETER

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException 
	{

		// set the MIME media type of the response
		res.setContentType("application/json; charset=utf-8");

		// get a stream to write the response
		OutputStream out = res.getOutputStream();

		try 
		{
		
			// retrieves the request parameter
			String PARAMETERNAME = req.getParameter("PARAMETERNAME");

			// creates a new object for accessing the database and searching the requested resource
			List<RESOURCE> RESOURCELISTVARIABLE = new SearchRESOURCEByPARAMETER(getDataSource().getConnection(), PARAMETERNAME)
					.SearchRESOURCEByPARAMETERDatabaseMETHOD();
			
			// write the list of result to the client as JSON
			new ResourceList<RESOURCE>(RESOURCELISTVARIABLE).toJSON(out);
			
		} 
		catch (SQLException ex) 
		{
			Message m = new Message("Cannot search for RESOURCE: unexpected error while accessing the database.", 
									"E100", ex.getMessage());
				
			res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			m.toJSON(out);
		}

		// flush the output stream buffer
		out.flush();

		// close the output stream
		out.close();
	}


	// EXAMPLE WITH INT PARAMETER

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		// set the MIME media type of the response
		res.setContentType("text/html; charset=utf-8");

		// get a stream to write the response
		PrintWriter out = res.getWriter();

		try 
		{

			// retrieves the request parameter
			int PARAMETERNAME = Integer.parseInt(req.getParameter("PARAMETERNAME"));
			
			// creates a new object for accessing the database and searching the requested resource
			List<RESOURCE> RESOURCELISTVARIABLE = new SearchRESOURCEByPARAMETER(getDataSource().getConnection(), PARAMETERNAME)
					.SearchRESOURCEByPARAMETERDatabaseMETHOD();

			// write the list of result to the client as JSON
			new ResourceList<RESOURCE>(RESOURCELISTVARIABLE).toJSON(out);


		} 
		catch (NumberFormatException ex) 
		{
			m = new Message("Cannot search for RESOURCE. Invalid input parameters: PARAMETERNAME must be integer.",
							"E200", ex.getMessage());

			res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			m.toJSON(out);

		} 
		catch (SQLException ex) 
		{
			m = new Message("Cannot search for RESOURCE: unexpected error while accessing the database.",
							"E100", ex.getMessage());

			res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			m.toJSON(out);

		}

		// flush the output stream buffer
		out.flush();

		// close the output stream
		out.close();
	}
}

