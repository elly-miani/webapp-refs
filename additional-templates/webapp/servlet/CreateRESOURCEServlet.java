package it.unipd.dei.webapp.servlet;

// import databases and resources
import it.unipd.dei.webapp.database.CREATERESOURCEDATABASE;
import it.unipd.dei.webapp.resource.RESOURCE;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public final class CreateRESOURCEServlet extends AbstractDatabaseServlet 
{

	public void doPost(HttpServletRequest req, HttpServletResponse res)
				throws ServletException, IOException 
	{
		
		// set the MIME media type of the response
		res.setContentType("application/json; charset=utf-8");

		// get a stream to write the response
		OutputStream out = res.getOutputStream();

	
		try 
		{
			final RESOURCE RESOURCEVARIABLE =  RESOURCE.fromJSON(req.getInputStream());

			// creates a new object for accessing the database and creating the resource
			RESOURCE r = new CreateRESOURCEDatabase(getDataSource().getConnection(), RESOURCEVARIABLE)
					.CreateRESOURCEDatabaseMETHOD();


			if ( r!= null) 
			{
				r.toJSON(out);
			} 
			else 
			{
				Message m = new Message("Cannot create RESOURCE: unexpected error.",
										"E100", null);

				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

				m.toJSON(out);
			}
		} 
		catch (Exception ex) 
		{
				Message m = new Message("Cannot create RESOURCE: unexpected error.",
						"E100", ex.getMessage());

				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

				m.toJSON(out);
		}

		// flush the output stream buffer
		out.flush();

		// close the output stream
		out.close();
	}

}
