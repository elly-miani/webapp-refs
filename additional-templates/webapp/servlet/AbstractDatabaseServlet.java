package it.unipd.dei.webapp.servlet;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.sql.DataSource;


public abstract class AbstractDatabaseServlet extends HttpServlet {

	// The connection pool to the database.
	private DataSource ds;

	// Gets the DataSource for managing the connection pool to the database.
	// Only a reference to the pool of connections is held in the servlet
	public void init(ServletConfig config) throws ServletException {

		// the JNDI lookup context
        InitialContext cxt;
	
		try 
        {
			// the JNDI directory to look up by using the name assigned to the JDBC pool in the
			// context.xml file. Any look up problem will throw an exception.
            cxt = new InitialContext();

			// the string is the name of the directory in tomcat
            ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/miani-1206908");
        } 
        catch (NamingException e) 
        {
            ds = null;

            throw new ServletException(
                    String.format("Impossible to access the connection pool to the database: %s",
                                  e.getMessage()));
        }
	}


	// Releases the DataSource for managing the connection pool to the database.
	public void destroy() {
        ds = null;
    }


    //  Returns the DataSource for managing the connection pool to the database.
	protected final DataSource getDataSource() {
        return ds;
    }
}
