package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.RESOURCE;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public final class SearchRESOURCEByPARAMETERDatabase 
{ 

	// The SQL statement to be executed
	// Select statement
	private static final String STATEMENT = "SELECT id, ATTRIBUTE1, ATTRIBUTE2" +
											"FROM DatabaseSchemaName.ResourceTable" +
											"WHERE DatabaseSchemaName.ResourceTable.PARAMETER = ?::DatabaaseSchemaName.Type";

	// The connection to the database
	private final Connection con;

	// The PARAMETER of the RESOURCE
	private final String PARAMETER;


	// Creates a new object for searching RESOURCE by PARAMETER
	public SearchRESOURCEByPARAMETERDatabase(final Connection con, final String PARAMETER) 
	{
		this.con = con;
		this.PARAMETER = PARAMETER;
	}


	// Searches RESOURCE by PARAMETER
	public List<RESOURCE> SearchRESOURCEByPARAMETER() throws SQLException 
	{

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<RESOURCE> RESOURCELIST = new ArrayList<RESOURCE>();

		try 
		{
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, category);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				RESOURCELIST.add(
					new RESOURCE(
								rs.getInt("id"), 
								rs.getString("ATTRIBUTE1"), 
								rs.getDouble("ATTRIBUTE2")
								)
							);
			}
		} 
		finally 
		{
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return RESOURCELIST;
	}
}
