package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.RESOURCE;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public final class CreateRESOURCEDatabase {


	// The SQL statement to be executed
	// Insert statement
	private static final String STATEMENT = "INSERT INTO DatabaseSchemaName.ResourceTable> (id, ATTRIBUTE1, ATTRIBUTE2)" +
											"VALUES (DEFAULT, ?, ?)" +
											" RETURNING *";
	
	// Delete statement
//	private static final String STATEMENT = "DELETE "
//                                            + " FROM <nomeDatabase>.<tabella>"
//                                            + " WHERE <nomeDatabase>.<tabella>.<attributo> = ?"
//										 	+ " RETURNING *";

	// The connection to the database
	private final Connection con;

	// The RESOURCE to create
	private final RESOURCE RESOURCEVARIABLE;


	// Creates a new object for creating RESOURCE
	public CreateRESOURCEDatabase(final Connection con, final RESOURCE RESOURCEVARIABLE) {
		this.con = con;
		this.RESOURCEVARIABLE = RESOURCEVARIABLE;
	}

	// Creates a new RESOURCE
	public RESOURCE createRESOURCE() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the creation
		RESOURCE r = null;

		try 
		{
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, RESOURCEVARIABLE.getATTRIBUTE1());
			pstmt.setDouble(2, RESOURCEVARIABLE.getATTRIBUTE2());

			rs = pstmt.executeQuery();

			while (rs.next()) {
				r = new RESOURCE(
								rs.getInt("id"), 
								rs.getString("ATTRIBUTE1"),
								rs.getDouble("ATTRIBUTE2")
								);
			}
		} 
		finally 
		{
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return r;
	}
}
