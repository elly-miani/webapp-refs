## Show table results from form request


### For search-RESOURCE-by-PARAMETER.jsp

```

<c:import url="/jsp/include/show-message.jsp"/>

<c:if test='${not emtpy RESOURCEList}'>
  <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>ATTRIBUTE1</th>
        <th>ATTRIBUTE2</th>
      </tr>
    </thead>
    <tbody>
      <!-- Iterate over each element on the list -->
      <c:forEach var="RESOURCE" items="${RESOURCEList}">
        <tr>
          <!-- Write the content of the RESOURCE object using the JavaBeans conventions -->
          <td><c:out value="${RESOURCE.id}"/></td>
          <td><c:out value="${RESOURCE.ATTRIBUTE1}"/></td>
          <td><c:out value="${RESOURCE.ATTRIBUTE2}"/></td>
        </tr>
      </c:forEach>
    </tbody>
  </table>
</c:if>
```


### For show-message.jsp

```
<!-- Set the Content-Type response header and use the Core taglib -->
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
  <!-- If it is an error message -->
  <c:when test="${message.error}">
    <ul>
      <!-- Write the message and additional error information -->
      <li>error code: <c:out value="${message.errorCode}"/></li>
      <li>message: <c:out value="${message.message}"/></li>
      <li>details: <c:out value="${message.errorDetails}"/></li>
    </ul>
  </c:when>

  <!-- else -->
  <c:otherwise>
    <!-- Write only the message -->
    <p><c:out value="${message.message}"/></p>
  </c:otherwise>
</c:choose>
```