## Get DOM elements

```
const FORMVARIABLE = document.getElementById("FORMID");
```

```
const ELEMENTNAME = document.getElementsByTagName("HTMLTAG")[0];
```




## AJAX CALL WITH GET METHOD
Specify `url` to make the call to, and functions `successHandler(jsonData)` and `errorHandler(jsonData)` to be called.


```
function ajax_get(url, successHandler, errorHandler) {

  var httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		alert('Cannot create an XMLHttpRequest instance.');
		return false;
	}

  httpRequest.onreadystatechange = function() {

		if (httpRequest.readyState === XMLHttpRequest.DONE) {

			if (httpRequest.status == 200) {
				// 200 status
				var jsonData = JSON.parse(httpRequest.responseText);
				successHandler(jsonData);
			}
			else {
				// error: pass status code to errorHandler function
				var jsonData = JSON.parse(httpRequest.responseText);
				errorHandler(jsonData, httpRequest.status);
			}
		}

  };

	httpRequest.open('GET', url);
	httpRequest.send();
}
```



## FORM SUMBIT HANDLING 

### When the json content returned is used to fill a table


```
FORMVARIABLE.addEventListener("submit", function(event) {

	event.preventDefault();

	FORMELEMENTNAME = document.getElementById("FORMELEMENTID").name;
	FORMELEMENTVALUE = document.getElementById("form-select-searchByCategory").value;

	url = FORMVARIABLE.action + '?' + FORMELEMENTNAME + '=' + FORMELEMENTVALUE;
	console.log(url);

	// clean up results and error messages
	RESULTSVARIABLE.innerHTML="";
	ERRORMESSAGEVARIABLE.innerHTML="";
	


	ajax_get(url,

		function(jsonData) {
			// 200 status

			jsonData = jsonData['MAINCONTENTJSON'];

			// for each element in the array MAINCONTENTJSON 
			for (var i=0; i<jsonData.length; i++) {

				ELEMENT = jsonData[i].ELEMENTNAMEJSON;

				var tr = document.createElement('tr');

				// for each attribute
				['ATTRIBUTE1','ATTRIBUTE1','ATTRIBUTE1','ATTRIBUTE1','ATTRIBUTE1'].forEach(function(attribute){

					var td = document.createElement('td');
					td.appendChild(document.createTextNode(product[attribute]));
					tr.appendChild(td);
				});

				tableResultsBody.appendChild(tr);
			}

			RESULTSVARIABLE.classList.remove('hidden');
		},

		function(jsonData, errorCode) {

			if (errorCode == 500) {
				// 500 status

				jsonData = jsonData['message'];

				var ul = document.createElement('ul');

				var li = document.createElement('li');
				li.appendChild(document.createTextNode('Message:' + jsonData['message']));
				ul.appendChild(li);

				li = document.createElement('div');
				li.appendChild(document.createTextNode('Error code:' + jsonData['error-code']));
				ul.appendChild(li);

				li = document.createElement('div');
				li.appendChild(document.createTextNode('Error details:' + jsonData['error-details']));
				ul.appendChild(li);


				ERRORMESSAGEVARIABLE.appendChild(ul);
				ERRORMESSAGEVARIABLE.classList.remove('hidden');
			}
			else {
				// generic error

				var p = document.createElement('p');

				p.appendChild(document.createTextNode('Unexpected Error'));

				ERRORMESSAGEVARIABLE.appendChild(p);
				ERRORMESSAGEVARIABLE.classList.remove('hidden');
			}

		}
	)

});
```
