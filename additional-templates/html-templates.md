## BASIC FORMS

```
<div id="search-results">
	<table id="table-search-results" class="table table-hover hidden">
		<thead>
			<tr>
				<th scope="col">ID</th>
				<th scope="col">Category</th>
				<th scope="col">Name</th>
				<th scope="col">Age Range</th>
				<th scope="col">Price</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<div id="error-message" class="hidden alert alert-danger"></div>
</div>

```
